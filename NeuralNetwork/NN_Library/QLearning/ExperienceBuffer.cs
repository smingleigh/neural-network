﻿using System.Collections;
using System.Collections.Generic;

namespace NN_Library
{
    public class ExperienceBuffer : IEnumerable<Experience>
    {
        private Queue<Experience> storage;
        public int Count { get; private set; } = 0;
        private readonly int capacity;
        private static readonly StrongRandom random = new StrongRandom();

        public ExperienceBuffer(int size)
        {
            storage = new Queue<Experience>();
            capacity = size;
        }

        public void Add(Experience item)
        {
            if (Count >= capacity)
                storage.Dequeue();
            else
                Count++;
            storage.Enqueue(item);
        }

        public IEnumerator<Experience> GetEnumerator() =>
            ((IEnumerable<Experience>)storage).GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() =>
            ((IEnumerable<Experience>)storage).GetEnumerator();
    }
}
