﻿namespace NN_Library
{
    public class Experience
    {
        public double[] Inputs { get; private set; }
        public double Cost { get; set; }
        public int Choice { get; private set; }

        public Experience(double[] inputs, int choice)
        {
            Inputs = inputs;
            Choice = choice;
        }

        private Experience(double[] inputs, int choice, double cost)
        {
            Inputs = inputs;
            Choice = choice;
            Cost = cost;
        }

        public Experience WithCost(double cost) =>
            new Experience(Inputs, Choice, cost);
    }
}
