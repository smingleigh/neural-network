﻿using System.Collections.Generic;
using System.Linq;

namespace NN_Library
{
    public class Memory
    {
        public ExperienceBuffer Short;
        public ExperienceBuffer Long;
        public ExperienceBuffer Traumatic;
        public ExperienceBuffer Pleasant;
        private int count = 0;
        private readonly int refresh;
        private readonly IRandom random = new WeakRandom();

        public Memory(int Short = 256, int Long = 512, int Pleasant = 64, int Traumatic = 64, int memoryRefresh = 32)
        {
            this.Short = new ExperienceBuffer(Short);
            this.Long = new ExperienceBuffer(Long);
            this.Pleasant = new ExperienceBuffer(Pleasant);
            this.Traumatic = new ExperienceBuffer(Traumatic);
            refresh = memoryRefresh;
        }

        public void Remember(Experience experience)
        {
            count++;
            Short.Add(experience);

            if (count % refresh == refresh - 1)
            {
                Long.Add(experience);
                var sorted = Short.OrderBy(o => o.Cost);
                Pleasant.Add(sorted.Last());
                Traumatic.Add(sorted.First());
            }
        }

        public IEnumerable<Experience> Recall(int count) =>
            Short.Concat(Traumatic).Concat(Pleasant).Concat(Long).Shuffle(random).Take(count).ToList();
    }
}
