﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NN_Library
{
    public class QLearn
    {
        private readonly NeuralNetwork neuralNetwork;
        private readonly Func<double, double> costFunction;
        private readonly double initialExploration = 1.0d;
        private readonly double finalExploration = 0.001d;
        private readonly double explorationDecay;
        private double currentExploration;
        private double runningCost = 0.25d;
        private readonly double predictability;
        private readonly int memoryWarmUp;
        private Memory memory = new Memory();
        private bool memoryReady = false;

        private Queue<Experience> learning = new Queue<Experience>();

        private readonly IRandom random = new StrongRandom();

        public QLearn(NeuralNetwork neuralNetwork, Func<double, double> costFunction, int lookAhead,  double initialExploration, double finalExploration, int explorationDecayPeriod, int memoryWarmUp = 4, double predictability = 0.8d)
        {
            this.neuralNetwork = neuralNetwork;
            this.costFunction = costFunction;
            this.initialExploration = initialExploration;
            this.finalExploration = finalExploration;
            this.explorationDecay = (initialExploration - finalExploration) / explorationDecayPeriod;
            this.currentExploration = initialExploration;
            this.predictability = predictability;
            this.memoryWarmUp = memoryWarmUp;
        }

        private int Explore(Matrix<double> input) =>
            neuralNetwork.Evaluate(input)
                         .ToColumnMajorArray()
                         .WeakWeightedChoice();

        public int Think(double[] input) =>
            Think(Matrix<double>.Build.DenseOfRowMajor(1, input.Length, input));
        
        public void UpdateCost(double currentCost) =>
            runningCost = (runningCost * predictability) + (currentCost * (1d - predictability));

        public int Think(Matrix<double> input)
        {
            int decision;
            if (random.Next <= currentExploration)
            {
                if (currentExploration > finalExploration)
                    currentExploration -= explorationDecay;
                decision = (int)(random.Next * neuralNetwork.Outputs);
            }
            else
                decision = Explore(input);

            learning.Enqueue(new Experience(input.ToColumnMajorArray(), decision));

            if (!memoryReady)
            {
                if (learning.AtLeast(memoryWarmUp))
                    memoryReady = true;
            }
            else
            {
                memory.Remember(learning.Dequeue().WithCost(runningCost));
                Learn(memory.Recall(memoryWarmUp));
            }

            return decision;
        }

        private void Learn(IEnumerable<Experience> experience) =>
            Learn(experience.Select(o => o.Inputs).ToMatrix(), experience.Select(o => o.Cost), experience.Select(o => o.Choice));

        private void Learn(Matrix<double> example, IEnumerable<double> costs, IEnumerable<int> choices) =>
            neuralNetwork.TrainByError(example, costs.Zip(choices, (cost, choice) => { double[] result = Enumerable.Repeat(0d, neuralNetwork.Outputs).ToArray(); result[choice] = costFunction(cost); return result; }).ToMatrix(neuralNetwork.Outputs));
    }
}
