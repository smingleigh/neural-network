﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NN_Library
{
    public static class Extensions
    {
        public static IEnumerable<T> Reversed<T>(this IEnumerable<T> source) =>
            new Stack<T>(source) as IEnumerable<T>;

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source) =>
            source.Shuffler();

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source, IRandom random) =>
            source.Shuffler(random);

        private static IEnumerable<T> Shuffler<T>(this IEnumerable<T> source)
        {
            return source.Shuffler(new StrongRandom());
        }

        private static IEnumerable<T> Shuffler<T>(this IEnumerable<T> source, IRandom rng)
        {
            var buffer = source.ToList();
            for (int i = 0; i < buffer.Count; i++)
            {
                int j = i + (int)(rng.Next * (buffer.Count() - i));
                yield return buffer[j];

                buffer[j] = buffer[i];
            }
        }

        public static IEnumerable<IEnumerable<T>> Batch<T>(this IEnumerable<T> source, int size)
        {
            T[] bucket = null;
            var count = 0;

            foreach (var item in source)
            {
                if (bucket == null)
                    bucket = new T[size];

                bucket[count++] = item;

                if (count != size)
                    continue;

                yield return bucket.Select(x => x);

                bucket = null;
                count = 0;
            }

            // Return the last bucket with all remaining elements
            if (bucket != null && count > 0)
                yield return bucket.Take(count);
        }

        private static readonly MatrixBuilder<double> builder = Matrix<double>.Build;

        public static Matrix<double> ToMatrix(this IEnumerable<double[]> source, int width)
        {
            var height = source.Count();
            var result = new double[width * height];
            int offset = 0;

            foreach (double[] array in source)
            {
                array.CopyTo(result, offset);
                offset += width;
            }

            return builder.Dense(height, width, result);
        }

        public static Matrix<double> ToMatrix(this IEnumerable<double[]> source) =>
            source.ToMatrix(source.First().Length);

        public static Matrix<double> ToMatrix(this double[] source) =>
            new[] { source }.ToMatrix(source.Length);

        private const int MIN = -1;
        private const int MAX = 1;

        private static T minMaxBy<T, TKey>(this IEnumerable<T> sequence, Func<T, TKey> selector, int MinMax)
            where T : class
            where TKey : IComparable
        {
            if (sequence.UpTo(1))
                return sequence.FirstOrDefault();
            return sequence.Aggregate(new { result = sequence.First(), value = selector(sequence.First()) },
                                (first, second) => selector(second).CompareTo(first.value) == MinMax
                                    ? new { result = second, value = selector(second) }
                                    : first)
                           .result;
        }
        public static T MinBy<T, TKey>(this IEnumerable<T> sequence, Func<T, TKey> selector)
            where T : class
            where TKey : IComparable =>
            minMaxBy(sequence, selector, MIN);

        public static T MaxBy<T, TKey>(this IEnumerable<T> sequence, Func<T, TKey> selector)
            where T : class
            where TKey : IComparable =>
            minMaxBy(sequence, selector, MAX);

        public static bool AtLeast<T>(this IEnumerable<T> sequence, int count) =>
            sequence.Take(count + 1).Count() >= count;

        public static bool AtLeastOne<T>(this IEnumerable<T> sequence) =>
            sequence.AtLeast(1);

        public static bool UpTo<T>(this IEnumerable<T> sequence, int count) =>
            sequence.Take(count + 1).Count() <= count;

        public static bool UpToOne<T>(this IEnumerable<T> sequence) =>
            sequence.UpTo(1);

        public static bool Exactly<T>(this IEnumerable<T> sequence, int count) =>
            sequence.Take(count + 1).Count() == count;

        public static bool One<T>(this IEnumerable<T> sequence) =>
            sequence.Exactly(1);

        public static IEnumerable<T> FirstOrEmpty<T>(this IEnumerable<T> sequence) =>
            sequence.Take(1);

        public static int StrongWeightedChoice(this IEnumerable<double> evaluation) =>
            evaluation.WeightedChoice(new StrongRandom());

        public static int WeakWeightedChoice(this IEnumerable<double> evaluation) =>
            evaluation.WeightedChoice(new WeakRandom());

        public static int WeightedChoice(this IEnumerable<double> evaluation, IRandom random)
        {
            double min = evaluation.Min();
            var weights = evaluation.Select((prediction, index) => new { weight = (prediction - min) + double.Epsilon, index }).OrderByDescending(o => o.weight);
            double choice = random.Next * weights.Sum(o => o.weight);
            foreach (var item in weights)
            {
                if (choice <= item.weight)
                    return item.index;
                choice -= item.weight;
            }
            return weights.First().index;
        }
    }
}