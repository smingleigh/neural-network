﻿using System;
using System.Security.Cryptography;

namespace NN_Library
{
    internal class StrongRandom : IRandom
    {
        private static RNGCryptoServiceProvider random;

        static StrongRandom()
        {
            random = new RNGCryptoServiceProvider();
        }

        public double Next
        {
            get
            {
                var bytes = new Byte[8];
                random.GetBytes(bytes);
                var ul = BitConverter.ToUInt64(bytes, 0) / (1 << 11);
                return ul / (double)(1UL << 53);
            }
        }

        public double Normal =>
            Next - Next;

        public double SignedSquareNormal
        {
            get
            {
                double val = Normal;
                return Math.Sign(val) * Math.Pow(val, 2);
            }
        }

        public double SquareNormal =>
            Math.Pow(Normal, 2);
    }
}
