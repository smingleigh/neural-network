﻿using System;

namespace NN_Library
{
    internal class WeakRandom : IRandom
    {
        private static Random random;

        static WeakRandom()
        {
            random = new Random();
        }

        public double Next =>
            random.Next();

        public double Normal =>
            random.Next() - random.Next();

        public double SignedSquareNormal
        {
            get
            {
                double val = Normal;
                return Math.Sign(val) * Math.Pow(val, 2);
            }
        }

        public double SquareNormal =>
            Math.Pow(Normal, 2);
    }
}
