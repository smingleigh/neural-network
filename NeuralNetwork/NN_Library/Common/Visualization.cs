﻿using System;
using System.Text;

namespace NN_Library
{
    public static class Visualization
    {
        private static readonly string asciiGradient = " :><~+-?][}{)(|/xcXYUJCLQOZ#MW&8%B@$";
        //private static readonly string asciiGradient = " .:-=+*#%@";
        private static readonly int maxIndex;

        static Visualization()
        {
            maxIndex = asciiGradient.Length - 1;
        }

        public static string Draw(double[] array, int width, int height)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < height; ++i)
            {
                for (int j = 0; j < width; ++j)
                {
                    sb.Append(asciiGradient[Math.Max(0, Math.Min((int)Math.Floor(maxIndex * array[i * width + j]), maxIndex))]);
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }
    }
}
