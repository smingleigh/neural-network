﻿namespace NN_Library
{
    public interface IRandom
    {
        double Next { get; }
        double Normal { get; }
        double SignedSquareNormal { get; }
        double SquareNormal { get; }
    }
}