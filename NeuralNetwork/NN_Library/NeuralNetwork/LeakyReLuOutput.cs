﻿namespace NN_Library
{
    public class LeakyReLuOutput : FullyConnectedLayer
    {
        public LeakyReLuOutput(int nodesIn, int nodesOut)
            : base(nodesIn, 
                   nodesOut,
                   d => d > double.Epsilon ? d : d / 10d,
                   d => d > double.Epsilon ? 1d : 0.1d)
        { }

        public override string ToString() =>
            $"Leaky Linear Rectification Output {base.ToString()}";
    }
}