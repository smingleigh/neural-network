﻿using System;

namespace NN_Library
{
    public class ReLuLayer : FullyConnectedLayer
    {
        public ReLuLayer(int nodesIn, int nodesOut)
            : base(nodesIn,
                   nodesOut, 
                   d => Math.Max(d, double.Epsilon), 
                   d => d > 0d ? 1d : 0d)
        { }

        public override string ToString() =>
            $"Linear Rectification {base.ToString()}";
    }
}