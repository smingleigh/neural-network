﻿using MathNet.Numerics.LinearAlgebra;
using System;

namespace NN_Library
{
    public abstract class Layer : ILayer
    {
        protected Matrix<double> weights;
        protected Matrix<double> lastOutput;
        protected Matrix<double> lastInput;
        protected Matrix<double> adjustment;
        protected Matrix<double> inputError;
        protected VectorBuilder<double> V = Vector<double>.Build;
        protected Func<double, double> activation;
        protected Func<double, double> gradient;
        public int Nodes { get; private set; }

        public Layer(int nodesIn, int nodesOut, Func<double, double> activation, Func<double, double> gradient)
        {
            weights = Matrix<double>.Build.Dense(nodesIn + 1, nodesOut);
            IRandom random = new StrongRandom();
            weights.MapInplace(_ => random.SignedSquareNormal / 2);

            adjustment = Matrix<double>.Build.Dense(nodesOut, nodesIn + 1, 0d);
            this.activation = activation;
            this.gradient = gradient;
            Nodes = nodesOut;
        }

        public abstract Matrix<double> Forward(Matrix<double> input);
        public abstract Matrix<double> Error(Matrix<double> outputError);
        public abstract Matrix<double> Backward(Matrix<double> input);

        public override string ToString() =>
            $"{weights.ColumnCount}x{weights.RowCount}";
    }
}