﻿using MathNet.Numerics.LinearAlgebra;
using System;

namespace NN_Library
{
    public abstract class FullyConnectedLayer : Layer
    {
        public FullyConnectedLayer(int nodesIn, int nodesOut, Func<double, double> activation, Func<double, double> gradient)
            : base(nodesIn, nodesOut, activation, gradient)
        { }

        public override Matrix<double> Forward(Matrix<double> input)
        {
            lastInput = input.InsertColumn(0, V.Dense(input.RowCount, 1d));
            lastOutput = lastInput * weights;
            return lastOutput.Map(activation);
        }

        public override Matrix<double> Error(Matrix<double> outputError)
        {
            adjustment = (lastOutput.Transpose().Map(gradient).PointwiseMultiply(outputError)) * NeuralNetwork.sigma * lastInput;
            inputError = Backward(outputError);
            weights.Add(adjustment.Transpose(), weights);

            return inputError;
        }

        public override Matrix<double> Backward(Matrix<double> input) =>
            (weights * input).RemoveRow(0);

        public override string ToString() =>
            $"{base.ToString()} (fully connected)";
    }
}