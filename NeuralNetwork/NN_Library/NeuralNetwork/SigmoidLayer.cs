﻿using System;

namespace NN_Library
{
    class SigmoidLayer: FullyConnectedLayer
    {
        public SigmoidLayer(int nodesIn, int nodesOut)
            : base(nodesIn,
                   nodesOut,
                   Sigmoid,
                   x => x * (1 - x))
        { }

        public override string ToString() =>
            $"Sigmoid {base.ToString()}";

        private static double Sigmoid(double x)
        {
            return 2 / (1 + Math.Exp(-2 * x)) - 1;
        }
    }
}
