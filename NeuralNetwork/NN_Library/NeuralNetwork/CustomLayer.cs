﻿using System;

namespace NN_Library
{
    class CustomLayer : FullyConnectedLayer
    {
        public CustomLayer(int nodesIn, int nodesOut, Func<double, double> activation, Func<double, double> gradient)
            : base(nodesIn, nodesOut, activation, gradient)
        { }

        public override string ToString() =>
            $"Custom Layer {base.ToString()}";
    }
}
