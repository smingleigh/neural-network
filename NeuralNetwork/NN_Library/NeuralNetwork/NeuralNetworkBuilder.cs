﻿using System.Collections.Generic;

namespace NN_Library
{
    public class NeuralNetworkBuilder
    {
        List<ILayer> layers = new List<ILayer>();
        int lastNodes;

        internal NeuralNetworkBuilder(int nodes)
        {
            lastNodes = nodes;
        }

        public NeuralNetworkBuilder ReLu(int nodes)
        {
            layers.Add(new ReLuLayer(lastNodes, nodes));
            lastNodes = nodes;
            return this;
        }

        public NeuralNetworkBuilder LeakyReLu(int nodes)
        {
            layers.Add(new LeakyReLuLayer(lastNodes, nodes));
            lastNodes = nodes;
            return this;
        }

        public NeuralNetworkBuilder Sigmoid(int nodes)
        {
            layers.Add(new SigmoidLayer(lastNodes, nodes));
            lastNodes = nodes;
            return this;
        }

        public NeuralNetwork LeakyReLuOutput(int nodes)
        {
            layers.Add(new LeakyReLuOutput(lastNodes, nodes));
            return new NeuralNetwork(layers);
        }
    }
}