﻿using MathNet.Numerics.LinearAlgebra;

namespace NN_Library
{
    public interface ILayer
    {
        int Nodes { get; }

        Matrix<double> Backward(Matrix<double> input);
        Matrix<double> Error(Matrix<double> outputError);
        Matrix<double> Forward(Matrix<double> input);
        string ToString();
    }
}