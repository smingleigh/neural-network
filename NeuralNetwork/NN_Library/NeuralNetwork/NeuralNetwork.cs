﻿using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NN_Library
{
    public class NeuralNetwork
    {
        internal static double sigma = 0.01d;

        private List<ILayer> layers;
        private readonly MatrixBuilder<double> M = Matrix<double>.Build;
        private readonly VectorBuilder<double> V = Vector<double>.Build;
        public int Outputs { get; private set; }

        public static NeuralNetworkBuilder Input(int nodes) =>
            new NeuralNetworkBuilder(nodes);

        internal NeuralNetwork(List<ILayer> layers)
        {
            Control.UseNativeMKL();
            this.layers = layers;
            Outputs = layers.Last().Nodes;
        }

        public NeuralNetwork LearningRate(double rate)
        {
            sigma = rate;
            return this;
        }

        public float Error { get; private set; } = 0f;
        public float ErrorSmoothing { get; set; } = 0.99f;

        private void _updateError(float currentError) =>
            Error = (Error * ErrorSmoothing) + ((1f - ErrorSmoothing) * currentError);

        private void _updateError(double currentError) =>
            _updateError((float)currentError);

        public double[,] Evaluate(double[,] input) =>
            Evaluate(M.DenseOfArray(input)).AsArray();

        public Matrix<double> Evaluate(Matrix<double> input) =>
            layers.Aggregate(input, (curr, next) => next.Forward(curr));

        // Make this return a Tuple<int, double> to represent the result & cost

        public IEnumerable<int> Categorize(double[,] input) =>
            Evaluate(M.DenseOfArray(input)).EnumerateRows().Select(o => o.MaximumIndex());

        public IEnumerable<int> Categorize(Matrix<double> input) =>
            Evaluate(input).EnumerateRows().Select(o => o.MaximumIndex());

        private void _backward(double[,] error) =>
            _backward(M.DenseOfArray(error));

        private void _backward(Matrix<double> error) =>
            layers.Reversed().Aggregate(error.Transpose(), (curr, next) => next.Error(curr)).ToArray();

        public Matrix<double> Hallucinate(Matrix<double> input) =>
            layers.Reversed().Aggregate(input.Transpose(), (curr, next) => next.Backward(curr));

        public double[] Hallucinate(double[] input) =>
            Hallucinate(input.ToMatrix()).ToColumnMajorArray();

        public void TrainByExample(double[,] example, double[,] actual) =>
            TrainByExample(M.DenseOfArray(example), M.DenseOfArray(actual));

        public void TrainByExample(double[,] example, double[,] actual, Func<double, double> costFunction) =>
            TrainByExample(M.DenseOfArray(example), M.DenseOfArray(actual), costFunction);

        public void TrainByExample(Matrix<double> example, Matrix<double> actual)
        {
            var error = (actual - Evaluate(example));
            _updateError(error.ColumnAbsoluteSums().Sum() / error.RowCount);
            _backward(error);
        }

        public void TrainByExample(Matrix<double> example, Matrix<double> actual, Func<double, double> costFunction)
        {
            var error = (actual - Evaluate(example)).Map(costFunction);
            _updateError(error.ColumnAbsoluteSums().Sum() / error.RowCount);
            _backward(error);
        }

        public void TrainByExample(IEnumerable<double[]> example, IEnumerable<double[]> actual) =>
            TrainByExample(example.ToMatrix(), actual.ToMatrix());

        public void TrainByExample(IEnumerable<double[]> example, IEnumerable<double[]> actual, Func<double, double> costFunction) =>
            TrainByExample(example.ToMatrix(), actual.ToMatrix(), costFunction);

        public void TrainByExample(double[] example, double[] actual) =>
            TrainByExample(example.ToMatrix(), actual.ToMatrix());

        public void TrainByExample(double[] example, double[] actual, Func<double, double> costFunction) =>
            TrainByExample(example.ToMatrix(), actual.ToMatrix(), costFunction);

        public void TrainByError(double[,] example, double[,] error) =>
            TrainByError(M.DenseOfArray(example), M.DenseOfArray(error));

        public void TrainByError(double[,] example, double[,] error, Func<double, double> costFunction) =>
            TrainByError(M.DenseOfArray(example), M.DenseOfArray(error), costFunction);

        public void TrainByError(IEnumerable<double[]> example, IEnumerable<double[]> error) =>
            TrainByError(example.ToMatrix(), error.ToMatrix());

        public void TrainByError(IEnumerable<double[]> example, IEnumerable<double[]> error, Func<double, double> costFunction) =>
            TrainByError(example.ToMatrix(), error.ToMatrix(), costFunction);

        public void TrainByError(Matrix<double> example, Matrix<double> error, Func<double, double> costFunction) =>
            TrainByError(example, error.Map(costFunction));

        public void TrainByError(Matrix<double> example, Matrix<double> error)
        {
            Evaluate(example);
            _updateError(error.ColumnAbsoluteSums().Sum() / error.RowCount);
            _backward(error.Transpose());
        }

        public void TrainByError(double[] example, double[] error) =>
            TrainByError(example.ToMatrix(), error.ToMatrix());

        public void TrainByError(double[] example, double[] error, Func<double, double> costFunction) =>
            TrainByExample(example.ToMatrix(), error.ToMatrix(), costFunction);
    }
}
