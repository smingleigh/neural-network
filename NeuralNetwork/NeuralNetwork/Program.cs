﻿using MathNet.Numerics.LinearAlgebra;
using NN_Library;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NNTest
{

    class Program
    {
        private static readonly MatrixBuilder<double> M = Matrix<double>.Build;

        static void Main(string[] args)
        {
            // Create the neural network
            NeuralNetwork digitEvaluator = NeuralNetwork.Input(28 * 28).LeakyReLu(35).LeakyReLuOutput(10).LearningRate(0.001d);

            // Set up training and test data
            IEnumerable<Digit> images = GetLabeledImages("mnistlabels.train", "mnist.train", 28, 28).Shuffle().ToList();
            int exampleCount = images.Count();
            Console.WriteLine($"Found {exampleCount} records.");
            var samples = images.Take((int)(exampleCount * 0.99d));
            IEnumerable<Digit> trainingSet = Enumerable.Repeat(samples, 20).Shuffle().SelectMany(o => o).ToList();
            Console.WriteLine($"Set up a test set of {(int)(exampleCount * 0.99d)} records.");
            IEnumerable<Digit> testSet = images.Skip((int)(exampleCount * 0.99d)).ToList().Shuffle().ToList();
            Console.WriteLine($"Set up a test set of {testSet.Count()} records.");

            // Learn
            int count = 0;
            int batch = 1;

            foreach (IEnumerable<Digit> imageBatch in trainingSet.Batch(batch))
            {
                count++;
                digitEvaluator.TrainByExample(imageBatch.Select(o => o.Pixels).ToMatrix(), 
                                              imageBatch.Select(o => o.Labels).ToMatrix(), 
                                              o => (o != 0d ? Math.Sign(o) : 1d) * Math.Pow(o, 2));

                if (count % 1000 == 0)
                    Console.WriteLine($"Error sum: {(digitEvaluator.Error).ToString("0.0000")} : {count * batch}");
            }

            // Show the results to the user

            foreach (Digit image in testSet.Take(3))
            {
                int result = digitEvaluator.Evaluate(new[] { image.Pixels }.ToMatrix(784)).EnumerateRows().First().MaximumIndex();
                Console.WriteLine(Visualization.Draw(image.Pixels, 28, 28));
                Console.WriteLine($"Evaluation: {result}");
                Console.WriteLine($"{(result == image.Label ? "***** CORRECT" : $"XXXXX WRONG - Should be {image.Label}")}");
                Console.ReadKey();
            }

            Console.WriteLine("Hallucinations! What would the network draw if it was asked to draw the numbers 0-9?");
            Console.ReadKey();
            foreach (int number in Enumerable.Range(0, 10))
            {
                double[] value = Enumerable.Repeat(0d, 10).ToArray();
                value[number] = 1d;
                double[] hallucination = digitEvaluator.Hallucinate(value);
                double max = hallucination.Select(o => (o != 0d ? Math.Sign(o) : 1d) * o * 8d).Max();
                Console.WriteLine(Visualization.Draw(hallucination.Select(o => o * 4d).ToArray(), 28, 28));
                Console.WriteLine($"Displaying {number}. The network evaluates the hallucination as {digitEvaluator.Evaluate(new[] { hallucination }.ToMatrix(28 * 28)).EnumerateRows().First().MaximumIndex()}");
                Console.ReadKey();
            }
        }

        private static IEnumerable<Digit> GetLabeledImages(string labelsFileName, string imagesFileName, int width, int height)
        {
            int elementLength = width * height;
            byte[] buffer = new byte[elementLength];
            using (FileStream ifsLabels = new FileStream($"..\\..\\{labelsFileName}", FileMode.Open))
            using (FileStream ifsImages = new FileStream($"..\\..\\{imagesFileName}", FileMode.Open))
            using (BinaryReader brLabels = new BinaryReader(ifsLabels))
            using (BinaryReader brImages = new BinaryReader(ifsImages))
            {
                brImages.ReadInt32(); // Irrelevant file details
                int numImages = brImages.ReadInt32();
                int numRows = brImages.ReadInt32();
                int numCols = brImages.ReadInt32();

                brLabels.ReadInt32(); // Irrelevant file details
                int numLabels = brLabels.ReadInt32();


                while (brImages.Read(buffer, 0, buffer.Length) > 0)
                    yield return new Digit(width, height, buffer, brLabels.ReadByte());
            }
        }
    }
}
