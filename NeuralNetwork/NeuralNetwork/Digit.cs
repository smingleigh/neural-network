﻿using System;
using System.Linq;

namespace NNTest
{
    public class Digit
    {
        public readonly double[] Pixels;
        public readonly double[] Labels;
        public readonly int Width;
        public readonly int Height;
        public readonly int Label;


        public Digit(int width, int height, byte[] pixels, byte label)
        {
            Pixels = pixels.Select(o => ((Convert.ToDouble(o) / 255d) * 2d) - 1d).ToArray();
            Labels = Enumerable.Repeat(0d, 10).ToArray();
            Labels[label] = 1d;

            Width = width;
            Height = height;
            Label = label;
        }
    }
}
